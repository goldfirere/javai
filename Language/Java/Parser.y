-- -*- haskell -*-
-- The line above tells emacs to use Haskell-style syntax highlighting

{
-----------------------------------------------------------------------------
-- |
-- Module      :  Language.Java.Parser
-- Copyright   :  (C) 2018 Richard Eisenberg
-- License     :  BSD-style (see LICENSE)
-- Maintainer  :  Richard Eisenberg (rae@cs.brynmawr.edu)
-- Stability   :  experimental
-- Portability :  portable
--
-- A Java parser
--
----------------------------------------------------------------------------

{- There are many stones left unturned here. There are principally two reasons
for the TODOs in this file.

1. Java is a big language, and it's simply not all here yet.

2. The grammar as presented in the JLS is ambiguous in an LALR parser. LALR means
that the parser reads tokens from left to right (that's the LR), with one token
of lookahead (that's the LA). It thus has to commit to a particular parsing choice
by looking only at the next token. The conflicts below describe problems this
poses for parsing Java.

conflict1: The problem is that all the following are valid:

  obj.field           // normal field access
  obj.field1.field2   // nested field access
  Type.this           // access of outer 'this' from inner class
  Outer.Inner.this    // access of outer 'this' when Inner is an inner class within Outer
  Type.class          // a "class" constant for a type
  Outer.Inner.class   // a "class" constant for Inner, an inner class of Outer
  ...                 // and a few more

When we get to the last dot, we need to decide if we're parsing something like the first
two examples, or something like the last 4. The ".this" and ".class" syntax are both
special syntax, treated differently from normal identifiers. So we have to make the
decision before the dot, and we can't.

The solution to this is to parse a list of dot-separated words into a list, then check
whether the last word is special. Not so hard, but I'd like to keep the parser here
closer to the JLS syntax, and so I'm not doing that now, as the forms I've eliminated
are somewhat exotic.

conflict2: Lambda expressions are hard to parse. Specifically, if we look
at "(x)", is that the beginning of a lambda? Or is it an expression in parentheses?
Impossible to know without more lookahead. Not sure offhand what the best solution
is here. Could ban this syntax (if there were a type there, it wouldn't be
ambiguous) or so something else creative. Deferring until later.

conflict3: This one is about telling the difference between casts and expressions.
If we have "(blah)", is that a parenthesized expression? Or is it the start of a cast?
Impossible to know. I think the solution here is to combine the ClassOrInterfaceType
parser with the QualifiedName parser. That's not too bad, but -- again -- I don't
want to stray from the JLS if possible. Right now, we don't support sub-typing yet,
so casting by a reference type isn't necessary.

conflict4: If we say "case Foo:" in a switch statement, is Foo an identifier
or a constant expression? The AST is different in these cases, and so this
causes a conflicts. For now, we don't permit constant expressions. The solution
is probably not to treat a single identifier specially in the parser.
-}

{-
Separate from the *removed* conflicts above, there remain some existing, more-or-less
intentional conflicts. These are all shift/reduce conflicts. These come up when
the parser is conflicted between ending a rule (reducing) or continuing a rule
(shifting). Shift/reduce conflicts are always resolved in favor of shifting: that is,
the rules consume as much of the input as possible. (Some call this "greedy"
parsing.) Because the resolution of the conflict is predictable, it's acceptable
to leave the conflict in.

Conflicts: None at this time.

-}

module Language.Java.Parser ( parseStatement, parseExpression ) where

import Language.Java.Identifier
import Language.Java.Literal
import Language.Java.Type
import Language.Java.Token
import Language.Java.Syntax
import Language.Java.Monad

import qualified Data.List.NonEmpty as NE
import Data.List.NonEmpty ( NonEmpty((:|)), cons )

}

%expect 0   -- should be no conflicts

%name parseStatement Statement
%name parseExpression Expression

%tokentype { Token }
%error { parseError }

%monad { Java }

%token
   -- keywords
-- TODO  'abstract'              { KeywordT AbstractK }
  'assert'                { KeywordT AssertK }
  'boolean'               { KeywordT BooleanK }
  'break'                 { KeywordT BreakK }
  'byte'                  { KeywordT ByteK }
  'case'                  { KeywordT CaseK }
  'catch'                 { KeywordT CatchK }
  'char'                  { KeywordT CharK }
-- TODO  'class'                 { KeywordT ClassK }
-- TODO  'const'                 { KeywordT ConstK }
  'continue'              { KeywordT ContinueK }
  'default'               { KeywordT DefaultK }
  'do'                    { KeywordT DoK }
  'double'                { KeywordT DoubleK }
  'else'                  { KeywordT ElseK }
-- TODO  'enum'                  { KeywordT EnumK }
  'extends'               { KeywordT ExtendsK }
  'final'                 { KeywordT FinalK }
  'finally'               { KeywordT FinallyK }
  'float'                 { KeywordT FloatK }
  'for'                   { KeywordT ForK }
  'if'                    { KeywordT IfK }
-- TODO  'goto'                  { KeywordT GotoK }
-- TODO  'implements'            { KeywordT ImplementsK }
-- TODO  'import'                { KeywordT ImportK }
  'instanceof'            { KeywordT InstanceofK }
  'int'                   { KeywordT IntK }
-- TODO  'interface'             { KeywordT InterfaceK }
  'long'                  { KeywordT LongK }
-- TODO  'native'                { KeywordT NativeK }
-- TODO  'new'                   { KeywordT NewK }
-- TODO  'package'               { KeywordT PackageK }
-- TODO  'private'               { KeywordT PrivateK }
-- TODO  'protected'             { KeywordT ProtectedK }
-- TODO  'public'                { KeywordT PublicK }
  'return'                { KeywordT ReturnK }
  'short'                 { KeywordT ShortK }
-- TODO  'static'                { KeywordT StaticK }
-- TODO  'strictfp'              { KeywordT StrictfpK }
  'super'                 { KeywordT SuperK }
  'switch'                { KeywordT SwitchK }
  'synchronized'          { KeywordT SynchronizedK }
  'this'                  { KeywordT ThisK }
  'throw'                 { KeywordT ThrowK }
-- TODO  'throws'                { KeywordT ThrowsK }
-- TODO  'transient'             { KeywordT TransientK }
  'try'                   { KeywordT TryK }
-- TODO  'void'                  { KeywordT VoidK }
-- TODO  'volatile'              { KeywordT VolatileK }
  'while'                 { KeywordT WhileK }

   -- separators
  '('                     { SeparatorT LParenS }
  ')'                     { SeparatorT RParenS }
  '{'                     { SeparatorT LBraceS }
  '}'                     { SeparatorT RBraceS }
  '['                     { SeparatorT LBracketS }
  ']'                     { SeparatorT RBracketS }
  ';'                     { SeparatorT SemicolonS }
  ','                     { SeparatorT CommaS }
  '.'                     { SeparatorT DotS }
-- TODO  '...'                   { SeparatorT EllipsisS }
-- TODO  '@'                     { SeparatorT AtS }
-- TODO  '::'                    { SeparatorT DoubleColonS }
  '<'                     { SeparatorT LAngleBracketS }   -- See Note [Parsing "<"] in Lexer.x

   -- operators
   '='                    { OperatorT AssignO }
   '=='                   { OperatorT EqualsO }
   '+'                    { OperatorT PlusO }
   '+='                   { OperatorT PlusEqualO }
   '>'                    { OperatorT GreaterO }
   '>='                   { OperatorT GreaterEqualO }
   '-'                    { OperatorT MinusO }
   '-='                   { OperatorT MinusEqualO }
   ' <'                   { OperatorT LessO }    -- See Note [Parsing "<"] in Lexer.x
   '<='                   { OperatorT LessEqualO }
   '*'                    { OperatorT TimesO }
   '*='                   { OperatorT TimesEqualO }
   '!'                    { OperatorT NotO }
   '!='                   { OperatorT NotEqualO }
   '/'                    { OperatorT DivideO }
   '/='                   { OperatorT DivideEqualO }
   '~'                    { OperatorT BinaryNotO }
   '&&'                   { OperatorT AndO }
   '&'                    { OperatorT BinaryAndO }
   '&='                   { OperatorT BinaryAndEqualO }
   '?'                    { OperatorT QuestionO }
   '||'                   { OperatorT OrO }
   '|'                    { OperatorT BinaryOrO }
   '|='                   { OperatorT BinaryOrEqualO }
   ':'                    { OperatorT ColonO }
   '++'                   { OperatorT PlusPlusO }
   '^'                    { OperatorT BinaryXorO }
   '^='                   { OperatorT BinaryXorEqualO }
-- TODO   '->'                   { OperatorT ArrowO }
   '--'                   { OperatorT MinusMinusO }
   '%'                    { OperatorT ModulusO }
     -- happy thinks %= is a happy command
   'm='                   { OperatorT ModulusEqualO }
   '<<'                   { OperatorT ShiftLeftO }
   '<<='                  { OperatorT ShiftLeftEqualO }
   '>>'                   { OperatorT ShiftRightO }
   '>>='                  { OperatorT ShiftRightEqualO }
   '>>>'                  { OperatorT ShiftRightZeroExtensionO }
   '>>>='                 { OperatorT ShiftRightZeroExtensionEqualO }

    -- identifiers
   IDENTIFIER             { IdentifierT $$ }

    -- literals
   LITERAL                { LiteralT $$ }

-- this ends the definition of tokens, and starts the parsing rules
%%

-------------------------------------------------
-- Identifiers

Identifier :: { Identifier }
Identifier : IDENTIFIER                   { Identifier $1 }
  -- NB: The "value" of an IDENTIFIER is the String that was parsed

------------------------------------------------
-- Literals

Literal :: { Literal }
Literal : LITERAL       { $1 }

------------------------------------------------
-- Types

-- S4.2
PrimitiveType :: { PrimitiveType }
PrimitiveType : NumericType { $1 }
              | 'boolean'   { Boolean }

NumericType :: { PrimitiveType }
NumericType : IntegralType      { $1 }
            | FloatingPointType { $1 }

IntegralType :: { PrimitiveType }
IntegralType : 'byte'    { Byte }
             | 'short'   { Short }
             | 'int'     { Int }
             | 'long'    { Long }
             | 'char'    { Char }

FloatingPointType :: { PrimitiveType }
FloatingPointType : 'float'   { Float }
                  | 'double'  { Double }

-- S4.3
-- Restructured to avoid conflicts. When a statement begins "A[", is that
-- an assignment to an array or a type? Impossible to tell.
ReferenceType :: { ReferenceType }
ReferenceType : ClassOrInterfaceType        { COIRT $1 }
              | ClassOrInterfaceArrayType   { ArrayRT $1 }
              | PrimitiveType Dims          { ArrayRT (PrimArray $1 $2) }

ClassOrInterfaceArrayType :: { ArrayType }
ClassOrInterfaceArrayType : QualifiedName '[' ']'
                              { COIArray (qnameToCOIType $1 []) (Dims 1) }
                          | QualifiedName '[' ']' Dims
                              { COIArray (qnameToCOIType $1 [])
                                         (case $4 of Dims n -> Dims (n+1)) }
                          | GenericCOIType Dims
                              { COIArray $1 $2 }

 -- NB: Including TypeVariable here led to redue/reduce conflicts,
 -- because there is no way to tell the difference between a class
 -- and a variable: String and E look the same to the parser.

-- InterfaceType is the same as ClassType for parsing, so this skips a
-- few steps
ClassOrInterfaceType :: { COIType }
-- This is rewritten as compared to the JLS to avoid shift/reduce conflicts;
-- there are places that variables can be confused with types. For example,
-- if "A.B" begins a statement, is that a type or variable?
-- Impossible to tell. This restructuring helps delay the choice.
ClassOrInterfaceType : QualifiedName                         { qnameToCOIType $1 []}
                     | GenericCOIType                        { $1 }

-- This version is guaranteed to have < ... > in it, making it distinguishable
-- from terms.
GenericCOIType :: { COIType }
GenericCOIType : QualifiedName TypeArguments           { qnameToCOIType $1 $2}
               | QualifiedName TypeArguments '.' ClassOrInterfaceType
                                       { qualifyCOIType (qnameToCOIType $1 $2) $4 }

Dims :: { Dims }
Dims : '[' ']'              { Dims 1 }
     | '[' ']' Dims         { case $3 of Dims n -> Dims (n+1) }

-- S4.4
{- TODO (conflict3)
AdditionalBound :: { COIType }
AdditionalBound : '&' ClassOrInterfaceType  { $2 }

AdditionalBounds :: { [COIType] }
AdditionalBounds : {- empty -}                      { [] }
                 | AdditionalBound AdditionalBounds { $1 : $2 }
-}

-- S4.5.1
TypeArguments :: { [TypeArgument] }
TypeArguments : '<' TypeArgumentList '>'     { $2 }

TypeArgumentList :: { [TypeArgument] }
TypeArgumentList : TypeArgument                      { [$1] }
                 | TypeArgument ',' TypeArgumentList { $1 : $3 }

TypeArgument :: { TypeArgument }
TypeArgument : ReferenceType          { ReferenceArgument $1 }
             | Wildcard               { WildcardArgument $1 }

Wildcard :: { Wildcard }
Wildcard : '?'                  { Wildcard Nothing }
         | '?' WildcardBounds   { Wildcard (Just $2) }

WildcardBounds :: { WildcardBounds }
WildcardBounds : 'extends' ReferenceType    { ExtendsWB $2 }
               | 'super' ReferenceType      { SuperWB $2 }

------------------------------------------------
-- Names

-- S6.5

-- See comments around declaration of QualifiedName for why this deviates from
-- the standard
QualifiedName :: { QualifiedName }
QualifiedName : ReversedDottedIdentifiersNE         { case $1 of id :| quals -> QualifiedName (reverse quals) id }

ReversedDottedIdentifiersNE :: { NonEmpty Identifier }  -- the resulting list is in *reverse* order
ReversedDottedIdentifiersNE : Identifier                                 { $1 :| [] }
                            | ReversedDottedIdentifiersNE '.' Identifier { $3 `cons` $1 }

------------------------------------------------
-- Declarations

-- S8.3
VariableDeclaratorList :: { NonEmpty VariableDeclarator }
VariableDeclaratorList : VariableDeclarator                            { $1 :| [] }
                       | VariableDeclarator ',' VariableDeclaratorList { $1 `cons` $3 }

VariableDeclarator :: { VariableDeclarator }
VariableDeclarator : VariableDeclaratorId    { VariableDeclarator $1 Nothing }
                   | VariableDeclaratorId '=' VariableInitializer
                                             { VariableDeclarator $1 (Just $3) }

VariableDeclaratorId :: { VariableDeclaratorId }
VariableDeclaratorId : Identifier            { VariableDeclaratorId $1 Nothing }
                     | Identifier Dims       { VariableDeclaratorId $1 (Just $2) }

VariableInitializer :: { VariableInitializer }
VariableInitializer : Expression             { ExpressionVI $1 }
-- TODO                    | ArrayInitializer       { ArrayInitializerVI $1 }

Type :: { Type }
Type : PrimitiveType      { PrimType $1 }
     | ReferenceType      { RefType $1 }

-- S8.4.1
VariableModifiers :: { [VariableModifier] }
VariableModifiers : {- empty -}                        { [] }
                  | VariableModifier VariableModifiers { $1 : $2 }

-- This factoring deviates from the standard so that the parser doesn't
-- have to decide when there is no 'final' in a few places, helping to
-- reduce conflicts. See usage sites.
VariableModifiers1 :: { [VariableModifier] }
VariableModifiers1 : VariableModifier                    { [$1] }
                   | VariableModifier VariableModifiers1 { $1 : $2 }

VariableModifier :: { VariableModifier }
VariableModifier : 'final'                             { FinalVM }

------------------------------------------------
-- Statements

-- S14.2
Block :: { Block }
Block : '{' '}'                    { Block [] }
      | '{' BlockStatements '}'    { Block (NE.toList $2) }

BlockStatements :: { NonEmpty BlockStatement }
BlockStatements : BlockStatement                   { $1 :| [] }
                | BlockStatement BlockStatements   { $1 `cons` $2 }

BlockStatement :: { BlockStatement }
BlockStatement : LocalVariableDeclarationStatement { LocalVariableDeclarationStatement $1 }
-- TODO               | ClassDeclaration                  { ClassDeclarationStatement $1 }
               | Statement                         { StatementBlock $1 }

-- S14.4
LocalVariableDeclarationStatement :: { LocalVariableDeclaration }
LocalVariableDeclarationStatement : LocalVariableDeclaration ';'  { $1 }

LocalVariableDeclaration :: { LocalVariableDeclaration }
LocalVariableDeclaration : Type VariableDeclaratorList
                             { LocalVariableDeclaration [] $1 $2 }
                         | VariableModifiers1 Type VariableDeclaratorList
                             { LocalVariableDeclaration $1 $2 $3 }

-- S14.5
Statement :: { Statement }
Statement : StatementWithoutTrailingSubstatement  { $1 }
          | LabeledStatement                      { $1 }
          | IfThenStatement                       { $1 }
          | IfThenElseStatement                   { $1 }
          | WhileStatement                        { $1 }
          | ForStatement                          { $1 }

StatementNoShortIf :: { Statement }
StatementNoShortIf : StatementWithoutTrailingSubstatement { $1 }
                   | LabeledStatementNoShortIf            { $1 }
                   | IfThenElseStatementNoShortIf         { $1 }
                   | WhileStatementNoShortIf              { $1 }
                   | ForStatementNoShortIf                { $1 }

StatementWithoutTrailingSubstatement :: { Statement }
StatementWithoutTrailingSubstatement
  : Block                       { BlockStatement $1 }
  | EmptyStatement              { $1 }
  | ExpressionStatement         { $1 }
  | AssertStatement             { $1 }
  | SwitchStatement             { $1 }
  | DoStatement                 { $1 }
  | BreakStatement              { $1 }
  | ContinueStatement           { $1 }
  | ReturnStatement             { $1 }
  | SynchronizedStatement       { $1 }
  | ThrowStatement              { $1 }
  | TryStatement                { $1 }

-- S14.6
EmptyStatement :: { Statement }
EmptyStatement : ';'   { EmptyStatement }

-- S14.7
LabeledStatement :: { Statement }
LabeledStatement : Identifier ':' Statement  { LabeledStatement $1 $3 }

LabeledStatementNoShortIf :: { Statement }
LabeledStatementNoShortIf : Identifier ':' StatementNoShortIf { LabeledStatement $1 $3 }

-- S14.8
ExpressionStatement :: { Statement }
ExpressionStatement : StatementExpression ';'   { ExpressionStatement $1 }

StatementExpression :: { Expression }
StatementExpression : Assignment                      { $1 }
                    | PreIncrementExpression          { $1 }
                    | PreDecrementExpression          { $1 }
                    | PostIncrementExpression         { $1 }
                    | PostDecrementExpression         { $1 }
-- TODO                    | MethodInvocation                { $1 }
-- TODO                    | ClassInstanceCreationExpression { $1 }

-- S14.9
IfThenStatement :: { Statement }
IfThenStatement : 'if' '(' Expression ')' Statement  { IfThenStatement $3 $5 Nothing }

IfThenElseStatement :: { Statement }
IfThenElseStatement : 'if' '(' Expression ')' StatementNoShortIf 'else' Statement
                               { IfThenStatement $3 $5 (Just $7) }

IfThenElseStatementNoShortIf :: { Statement }
IfThenElseStatementNoShortIf : 'if' '(' Expression ')' StatementNoShortIf 'else' StatementNoShortIf
                               { IfThenStatement $3 $5 (Just $7) }

-- S14.10
AssertStatement :: { Statement }
AssertStatement : 'assert' Expression ';'                 { AssertStatement $2 Nothing }
                | 'assert' Expression ':' Expression ';'  { AssertStatement $2 (Just $4) }

-- S14.11

-- Refactored to avoid conflicts. The JLS version requires guessing when we're
-- in the final SwitchLabels.
SwitchStatement :: { Statement }
SwitchStatement : 'switch' '(' Expression ')' '{' SwitchBlockStatementGroupList '}'
                    { SwitchStatement $3 (fst $6) (snd $6) }

SwitchBlockStatementGroupList :: { ([SwitchBlockStatementGroup], [SwitchLabel]) }
SwitchBlockStatementGroupList : {- empty -}                { ([], []) }
                              | SwitchBlockStatementGroup SwitchBlockStatementGroupList
                                                           { ($1 : fst $2, snd $2) }
                              | SwitchLabels               { ([], NE.toList $1) }

SwitchBlockStatementGroup :: { SwitchBlockStatementGroup }
SwitchBlockStatementGroup : SwitchLabels BlockStatements   { SwitchBlockStatementGroup $1 $2 }

SwitchLabels :: { NonEmpty SwitchLabel }
SwitchLabels : SwitchLabel              { $1 :| [] }
             | SwitchLabel SwitchLabels { $1 `cons` $2 }

SwitchLabel :: { SwitchLabel }
SwitchLabel : 'case' Identifier ':'           { EnumSL $2 }
-- TODO (conflict4)            | 'case' ConstantExpression ':'   { ConstantSL $2 }
            | 'default' ':'                   { DefaultSL }

-- S14.12
WhileStatement :: { Statement }
WhileStatement : 'while' '(' Expression ')' Statement { WhileStatement $3 $5 }

WhileStatementNoShortIf :: { Statement }
WhileStatementNoShortIf : 'while' '(' Expression ')' StatementNoShortIf
                                                      { WhileStatement $3 $5 }

-- S14.13
DoStatement :: { Statement }
DoStatement : 'do' Statement 'while' '(' Expression ')' ';'   { DoStatement $2 $5 }

-- S14.14
ForStatement :: { Statement }
ForStatement : BasicForStatement     { $1 }
             | EnhancedForStatement  { $1 }

ForStatementNoShortIf :: { Statement }
ForStatementNoShortIf : BasicForStatementNoShortIf    { $1 }
                      | EnhancedForStatementNoShortIf { $1 }

-- S14.14.1
BasicForStatement :: { Statement }
BasicForStatement : 'for' '(' ';' ';' ForUpdateOpt ')' Statement
                       { ForStatement Nothing Nothing $5 $7 }
                  | 'for' '(' ';' Expression ';' ForUpdateOpt ')' Statement
                       { ForStatement Nothing (Just $4) $6 $8 }
                  | 'for' '(' ForInit ';' ';' ForUpdateOpt ')' Statement
                       { ForStatement (Just $3) Nothing $6 $8 }
                  | 'for' '(' ForInit ';' Expression ';' ForUpdateOpt ')' Statement
                       { ForStatement (Just $3) (Just $5) $7 $9 }

BasicForStatementNoShortIf :: { Statement }
BasicForStatementNoShortIf
                  : 'for' '(' ';' ';' ForUpdateOpt ')' StatementNoShortIf
                       { ForStatement Nothing Nothing $5 $7 }
                  | 'for' '(' ';' Expression ';' ForUpdateOpt ')' StatementNoShortIf
                       { ForStatement Nothing (Just $4) $6 $8 }
                  | 'for' '(' ForInit ';' ';' ForUpdateOpt ')' StatementNoShortIf
                       { ForStatement (Just $3) Nothing $6 $8 }
                  | 'for' '(' ForInit ';' Expression ';' ForUpdateOpt ')' StatementNoShortIf
                       { ForStatement (Just $3) (Just $5) $7 $9 }

ForInit :: { ForInit }
ForInit : StatementExpressionList  { ExpressionsFI $1 }
        | LocalVariableDeclaration { LocalVariableDeclarationFI $1 }

ForUpdateOpt :: { [Expression] }
ForUpdateOpt : {- empty -}              { [] }
             | StatementExpressionList  { NE.toList $1 }

StatementExpressionList :: { NonEmpty Expression }
StatementExpressionList : StatementExpression                             { $1 :| [] }
                        | StatementExpression ',' StatementExpressionList { $1 `cons` $3 }

-- S14.14.2
EnhancedForStatement :: { Statement }
EnhancedForStatement : 'for' '(' Type VariableDeclaratorId ':' Expression ')' Statement
                        { EnhancedForStatement [] $3 $4 $6 $8 }
                     | 'for' '(' VariableModifiers1 Type VariableDeclaratorId ':' Expression ')' Statement
                        { EnhancedForStatement $3 $4 $5 $7 $9 }

EnhancedForStatementNoShortIf :: { Statement }
EnhancedForStatementNoShortIf
                     : 'for' '(' Type VariableDeclaratorId ':' Expression ')' StatementNoShortIf
                        { EnhancedForStatement [] $3 $4 $6 $8 }
                     | 'for' '(' VariableModifiers1 Type VariableDeclaratorId ':' Expression ')' StatementNoShortIf
                        { EnhancedForStatement $3 $4 $5 $7 $9 }

-- S14.15
BreakStatement :: { Statement }
BreakStatement : 'break' ';'            { BreakStatement Nothing }
               | 'break' Identifier ';' { BreakStatement (Just $2) }

-- S14.16
ContinueStatement :: { Statement }
ContinueStatement : 'continue' ';'              { ContinueStatement Nothing }
                  | 'continue' Identifier ';'   { ContinueStatement (Just $2) }

-- S14.17
ReturnStatement :: { Statement }
ReturnStatement : 'return' ';'                  { ReturnStatement Nothing }
                | 'return' Expression ';'       { ReturnStatement (Just $2) }

-- S14.18
ThrowStatement :: { Statement }
ThrowStatement : 'throw' Expression ';'         { ThrowStatement $2 }

-- S14.19
SynchronizedStatement :: { Statement }
SynchronizedStatement : 'synchronized' '(' Expression ')' Block  { SynchronizedStatement $3 $5 }

-- S14.20
TryStatement :: { Statement }
TryStatement : 'try' Block Catches              { TryStatement (Try $2 $3) }
             | 'try' Block Finally              { TryStatement (FinallyTry $2 [] $3) }
             | 'try' Block Catches Finally      { TryStatement (FinallyTry $2 (NE.toList $3) $4) }
             | TryWithResourcesStatement        { TryStatement $1 }

Catches :: { NonEmpty CatchClause }
Catches : CatchClause          { $1 :| [] }
        | CatchClause Catches  { $1 `cons` $2 }

CatchClause :: { CatchClause }
CatchClause : 'catch' '(' VariableModifiers CatchType VariableDeclaratorId ')' Block
                { CatchClause $3 (CatchType $4) $5 $7 }

CatchType :: { NonEmpty ClassType }
CatchType : ClassOrInterfaceType                  { $1 :| [] }
          | ClassOrInterfaceType '|' CatchType    { $1 `cons` $3 }

Finally :: { Block }
Finally : 'finally' Block     { $2 }

-- S14.20.3
TryWithResourcesStatement :: { Try }
TryWithResourcesStatement : 'try' ResourceSpecification Block
                             { ResourcesTry $2 $3 [] Nothing }
                          | 'try' ResourceSpecification Block Finally
                             { ResourcesTry $2 $3 [] (Just $4) }
                          | 'try' ResourceSpecification Block Catches
                             { ResourcesTry $2 $3 (NE.toList $4) Nothing }
                          | 'try' ResourceSpecification Block Catches Finally
                             { ResourcesTry $2 $3 (NE.toList $4) (Just $5) }

-- This is refactored ever so slightly to avoid conflicts around a final ';'.
ResourceSpecification :: { NonEmpty Resource }
ResourceSpecification : '(' ResourceList ')'      { $2 }

ResourceList :: { NonEmpty Resource }
ResourceList : Resource                   { $1 :| [] }
             | Resource ';'               { $1 :| [] }
             | Resource ';' ResourceList  { $1 `cons` $3 }

Resource :: { Resource }
Resource : VariableModifiers Type VariableDeclaratorId '=' Expression
            { Resource $1 $2 $3 $5 }

------------------------------------------------
-- Expressions

-- S15.2
Expression :: { Expression }
Expression : AssignmentExpression         { $1 }
-- TODO (conflict2)          | LambdaExpression             { $1 }


-- S15.8
Primary :: { Expression }
Primary : PrimaryNoNewArray       { $1 }
-- TODO        | ArrayCreationExpression { $1 }

PrimaryNoNewArray :: { Expression }
PrimaryNoNewArray : Literal                   { LiteralE $1 }
-- TODO (conflict1)                  | ClassLiteral              { ClassLiteralE $1 }
                  | 'this'                    { ThisE NoTQ }
-- TODO (conflict1)                  | QualifiedName '.' 'this'  { ThisE (TypeNameTQ $1) }
                  | '(' Expression ')'        { $2 }
-- TODO                  | ClassInstanceCreationExpression { ClassInstanceCreationE }
                  | FieldAccess               { FieldAccessE $1 }
                  | ArrayAccess               { ArrayAccessE $1 }
-- TODO                  | MethodInvocation          { $1 }
-- TODO                  | MethodReference           { $1 }

{- TODO (conflict1)
-- S15.8.2
ClassLiteral :: { ClassLiteral }
ClassLiteral : QualifiedName '.' 'class'            { TypeNameCL $1 Nothing }
             | QualifiedName Dims '.' 'class'       { TypeNameCL $1 (Just $2) }
             | PrimitiveType '.' 'class'            { PrimitiveTypeCL $1 Nothing }
             | PrimitiveType Dims '.' 'class'       { PrimitiveTypeCL $1 (Just $2) }
             | 'void' '.' 'class'                   { VoidCL }
-}

-- S15.10.3
ArrayAccess :: { ArrayAccess }
ArrayAccess : QualifiedName '[' Expression ']'      { ArrayAccess (NameE $1) $3 }
            | PrimaryNoNewArray '[' Expression ']'  { ArrayAccess $1 $3 }

-- S15.11
FieldAccess :: { FieldAccess }
FieldAccess : Primary '.' Identifier                   { FieldAccess (ExpressionFAQ $1) $3 }
            | 'super' '.' Identifier                   { FieldAccess (SuperFAQ NoTQ) $3 }
-- TODO (conflict1)            | QualifiedName '.' 'super' '.' Identifier { FieldAccess (SuperFAQ (TypeNameTQ $1)) $5 }

-- S15.14
PostfixExpression :: { Expression }
PostfixExpression : Primary                    { $1 }
                  | QualifiedName              { NameE $1 }
                  | PostIncrementExpression    { $1 }
                  | PostDecrementExpression    { $1 }

-- S15.14.2
PostIncrementExpression :: { Expression }
PostIncrementExpression : PostfixExpression '++'   { UnaryE PostIncrement $1 }

-- S15.14.3
PostDecrementExpression :: { Expression }
PostDecrementExpression : PostfixExpression '--'   { UnaryE PostDecrement $1 }

-- S15.15
UnaryExpression :: { Expression }
UnaryExpression : PreIncrementExpression      { $1 }
                | PreDecrementExpression      { $1 }
                | '+' UnaryExpression         { UnaryE UnaryPlus $2 }
                | '-' UnaryExpression         { UnaryE Negation $2 }
                | UnaryExpressionNotPlusMinus { $1 }

PreIncrementExpression :: { Expression }
PreIncrementExpression : '++' UnaryExpression { UnaryE PreIncrement $2 }

PreDecrementExpression :: { Expression }
PreDecrementExpression : '--' UnaryExpression { UnaryE PreDecrement $2 }

UnaryExpressionNotPlusMinus :: { Expression }
UnaryExpressionNotPlusMinus : PostfixExpression   { $1 }
                            | '~' UnaryExpression { UnaryE BitwiseNot $2 }
                            | '!' UnaryExpression { UnaryE LogicalNot $2 }
                            | CastExpression      { $1 }

-- S15.16
CastExpression :: { Expression }
CastExpression : '(' PrimitiveType ')' UnaryExpression { CastE (PrimCastType $2) $4 }
-- TODO (conflict3)               | '(' ReferenceType AdditionalBounds ')' UnaryExpressionNotPlusMinus
-- TODO (conflict3)                                                       { CastE (RefCastType $2 $3) $5 }
-- TODO (conflict2)               | '(' ReferenceType AdditionalBounds ')' LambdaExpression
-- TODO (conflict2)                                                       { CastE (RefCastType $2 $3) $5 }

-- S15.17
MultiplicativeExpression :: { Expression }
MultiplicativeExpression : UnaryExpression    { $1 }
                         | MultiplicativeExpression '*' UnaryExpression
                                              { BinaryE $1 Times $3 }
                         | MultiplicativeExpression '/' UnaryExpression
                                              { BinaryE $1 DividedBy $3 }
                         | MultiplicativeExpression '%' UnaryExpression
                                              { BinaryE $1 Modulus $3 }

-- S15.18
AdditiveExpression :: { Expression }
AdditiveExpression : MultiplicativeExpression       { $1 }
                   | AdditiveExpression '+' MultiplicativeExpression
                                                    { BinaryE $1 Plus $3 }
                   | AdditiveExpression '-' MultiplicativeExpression
                                                    { BinaryE $1 Minus $3 }

-- S15.19
ShiftExpression :: { Expression }
ShiftExpression : AdditiveExpression                       { $1 }
                | ShiftExpression '<<' AdditiveExpression  { BinaryE $1 LeftShift $3 }
                | ShiftExpression '>>' AdditiveExpression  { BinaryE $1 RightShift $3 }
                | ShiftExpression '>>>' AdditiveExpression { BinaryE $1 RightShiftZeroExtension $3 }

-- S15.20
RelationalExpression :: { Expression }
RelationalExpression : ShiftExpression      { $1 }
                        -- NB: ' <', not '<' in this line. See Note [Parsing "<"] in Lexer.x
                     | RelationalExpression ' <' ShiftExpression
                                            { BinaryE $1 LessThan $3 }
                     | RelationalExpression '>' ShiftExpression
                                            { BinaryE $1 GreaterThan $3 }
                     | RelationalExpression '<=' ShiftExpression
                                            { BinaryE $1 LessThanEquals $3 }
                     | RelationalExpression '>=' ShiftExpression
                                            { BinaryE $1 GreaterThanEquals $3 }
                     | RelationalExpression 'instanceof' ReferenceType
                                            { InstanceofE $1 $3 }

-- S15.21
EqualityExpression :: { Expression }
EqualityExpression : RelationalExpression        { $1 }
                   | EqualityExpression '==' RelationalExpression
                                                 { BinaryE $1 Equality $3 }
                   | EqualityExpression '!=' RelationalExpression
                                                 { BinaryE $1 Disequality $3 }

-- S15.22
AndExpression :: { Expression }
AndExpression : EqualityExpression                      { $1 }
              | AndExpression '&' EqualityExpression    { BinaryE $1 BitwiseAnd $3 }

ExclusiveOrExpression :: { Expression }
ExclusiveOrExpression : AndExpression                             { $1 }
                      | ExclusiveOrExpression '^' AndExpression   { BinaryE $1 BitwiseExclusiveOr $3 }

InclusiveOrExpression :: { Expression }
InclusiveOrExpression : ExclusiveOrExpression    { $1 }
                      | InclusiveOrExpression '|' ExclusiveOrExpression
                                                 { BinaryE $1 BitwiseOr $3 }

-- S15.23
ConditionalAndExpression :: { Expression }
ConditionalAndExpression : InclusiveOrExpression    { $1 }
                         | ConditionalAndExpression '&&' InclusiveOrExpression
                                                    { BinaryE $1 LogicalAnd $3 }

-- S15.24
ConditionalOrExpression :: { Expression }
ConditionalOrExpression : ConditionalAndExpression  { $1 }
                        | ConditionalOrExpression '||' ConditionalAndExpression
                                                    { BinaryE $1 LogicalOr $3 }

-- S15.26
ConditionalExpression :: { Expression }
ConditionalExpression : ConditionalOrExpression    { $1 }
                      | ConditionalOrExpression '?' Expression ':' ConditionalExpression
                                                   { ConditionalE $1 $3 $5 }
-- TODO (conflict2)                     | ConditionalOrExpression '?' Expression ':' LambdaExpression
-- TODO (conflict2)                                                  { ConditionalE $1 $3 $5 }

-- S15.26
AssignmentExpression :: { Expression }
AssignmentExpression : ConditionalExpression  { $1 }
                     | Assignment             { $1 }

Assignment :: { Expression }
Assignment : LeftHandSide AssignmentOperator Expression  { AssignmentE $1 $2 $3 }

LeftHandSide :: { LeftHandSide }
LeftHandSide : QualifiedName     { ExpressionNameLHS $1 }
             | FieldAccess       { FieldAccessLHS $1 }
             | ArrayAccess       { ArrayAccessLHS $1 }

AssignmentOperator :: { AssignmentOperator }
AssignmentOperator : '='    { Assigns }
                   | '*='   { TimesAssigns }
                   | '/='   { DividedByAssigns }
                   | 'm='   { ModulusAssigns }
                   | '+='   { PlusAssigns }
                   | '-='   { MinusAssigns }
                   | '<<='  { ShiftLeftAssigns }
                   | '>>='  { ShiftRightAssigns }
                   | '>>>=' { ShiftRightZeroExtensionAssigns }
                   | '&='   { BitwiseAndAssigns }
                   | '^='   { BitwiseExclusiveOrAssigns }
                   | '|='   { BitwiseOrAssigns }

{- TODO (conflict2)

-- S15.27
LambdaExpression :: { Expression }
LambdaExpression : LambdaParameters '->' LambdaBody   { LambdaE $1 $3 }

-- S15.27.1
LambdaParameters :: { LambdaParameters }
LambdaParameters : Identifier                   { IdentifierLP $1 }
                 | '(' ')'                      { FormalParameterListLP [] }
-- TODO                 | '(' FormalParameterList ')'  { FormalParameterListLP $2 }
                 | '(' Identifiers1 ')'         { InferredFormalParameterListLP $2 }

-- A comma-separated list containing at least one element
Identifiers1 :: { [Identifier] }
Identifiers1 : Identifier                     { [$1] }
             | Identifier ',' Identifiers1    { $1 : $3 }

LambdaBody :: { LambdaBody }
LambdaBody : Expression       { ExpressionLB $1 }
-- TODO           | Block            { BlockLB $1 }
-}

-- S15.28
{- TODO (conflict4)
ConstantExpression :: { Expression }
ConstantExpression : Expression   { $1 }
-}

-- more Haskell code that's part of the parser
{
qnameToCOIType :: QualifiedName -> [TypeArgument] -> COIType
qnameToCOIType (QualifiedName prefix suffix) args
  = COIType (idListToCOIType_maybe prefix) suffix args

idListToCOIType_maybe :: [Identifier] -> Maybe COIType
idListToCOIType_maybe ids = go (reverse ids)
  where
    go []     = Nothing
    go (n:ns) = Just (COIType (go ns) n [])

qualifyCOIType :: COIType -> COIType -> COIType
qualifyCOIType qual (COIType Nothing name args) = COIType (Just qual) name args
qualifyCOIType qual (COIType (Just q) name args)
  = COIType (Just (qualifyCOIType qual q)) name args

parseError :: [Token] -> Java a
parseError []   = issueError $ "Unexpected end of parse."
parseError toks = issueError $ "Parsing error when list of remaining tokens starts with:\n" ++
                               show (take 5 toks)
}
