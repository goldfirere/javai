// Tests for +
3 + 4
  :t 3 + 4
    3 + 4.
    :t 3 + 4.
      :t 3 + 4.f
	1.2f + 1.2f + 1.2f
	:t 3 + 4l + 5d
	  "hi" + 4
	  4 + "hi"
	  :t 4 + "hi"
	  4 + 5 + "hi"
	  4 + "hi" + 5
	  "hi" + 4 + 5
	  null + 4
	  4 + null
	  "hi" + null
	  null + "hi"
	  null + null
	  :t null + null
	  true + false
	  :t true + false
