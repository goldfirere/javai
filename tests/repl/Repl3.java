:{
  x = 3;
    :}
:{
  /* nothing to see here */
  :}
:{
  /* unterminated comment
:}
:{ // comment on first line
// more commentary
x = 34f;
:}
:{ // comment :}
x = true;
:}
