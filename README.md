The `javai` interpreter
=======================

This is an implementation of an interpreter for Java SE 8. The implementation
is meant to hew closely to the definitions in the (Java Language
Specification)[https://docs.oracle.com/javase/specs/jls/se8/html/] for Java SE 8.
Section references (beginning with an "S") refer to that document.

No attempt is made to make this interpreter performant, but it is the intention
that Java code will not be asymptotically slower in this interpreter than in
a normal Java implementation.

This implementation is meant to be used as a starting point for classroom exercises
in an introductory programming language course.

Deviations from the standard
----------------------------

As a toy(ish) implementation, there are naturally many features of Java that
are not supported. While wholly unimplemented features are too many to list,
the bullets below outline aspects of *implemented* features that are not
standards-conforming.

* **Packages/modules**: `javai` knows no notion of packages or modules. The
  system classes (such as `String`) are in scope from the launch of the interpreter.
  Note that the class is `String` and not `java.lang.String`, as the latter would
  involve a notion of "package". Loading a file into the interpreter brings
  its top-level classes/interfaces/etc into global scope. If file `B.java` depends
  on `A.java`, then load `A.java` first. `B.java` does *not* need to `import A`.

    If a file loaded into `javai` has an `import` statement, say, `import C`, it will
    look for a file `C.java` and load it (if `C` is not already loaded). If `C` is in
    the process of being loaded (in the case of mutually dependent files), the `import`
    is an error.

    Any `package` declaration is an error.

    Note that this scheme does not support mutually dependent files.

* **Integer literals**: Integer literals may overflow without causing a load-time error.

* **Floating-point literals**: No effort is made to conform to the IEEE standards
  regarding floating-point literals.

* **String literals**: No effort is made at commoning up `String` literals nor
  at identifying constant `String`-building expressions. All occurrences of
  a `String` (including as a loop executes!) will allocate a new `String` in
  memory.

* **Floating-point values**: Java's "value set conversion" (S5.1.13) is not
  implemented. Any `strictfp` keywords are simply ignored (but are parsed).
  See also S15.4.

* **Annotations**: No annotations are currently supported, including in parsing.
  Why remove them from parsing? Because they introduce ambiguities in a LALR
  parser.

* **Parsing**: Java has a well-known parsing ambiguity in a LALR parser around
`<`. If we, for example, `meth(T<A,B>`, is the argument the beginning of, say,
a method reference (like `T<A,B>::methRef`) or two comparison expressions
(like `T < A, B > C + D`)? It's impossible to know until we get to the `::` or
`+`, in these cases. These distinguishers can be arbitrarily far to the right
in the expression. We could come up with an inventive scheme for how to deal
with this (which is what [Eclipse](http://www.eclipse.org/jdt/core/howto/generate%20parser/generateParser.html)
has done to get to LALR), but instead, I chose the easy route: brutally require
that the less-than operator be preceded by whitespace, and the type-argument
angle bracket not be preceded by whitespace. This rule is simple, conforms
to standard style guidelines, and makes the parser look much closer to the
standard than it otherwise would.

* **String concatenation**: Normally, Java's `+` operator is type-directed,
using the compile-time analysis to distinguish between addition and concatentation.
This analysis must happen at compile-time, because, say, `(String)null + 4` should
be `"null4"`. A runtime analysis wouldn't be able to know that `null` denotes a
`String`. We want to be able to experiment without a type checker, though, and
so this kind of type-directed behavior is unfortunate. In this implementation,
therefore, the `+` operator is runtime-type-directed. In other words, we look
to see if either operand is a reference to a `String` object and make a decision
accordingly. In `(String)null + 4`, we'd see no `String` references and
try to do an unboxed conversion on the `null`, getting a `NullPointerException`.
In practice, null `String`s are rare, and so I don't expect this change to bite.
It wouldn't be all that hard to change if need be.
