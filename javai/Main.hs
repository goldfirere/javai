-----------------------------------------------------------------------------
-- |
-- Module      :  Main
-- Copyright   :  (C) 2018 Richard Eisenberg
-- License     :  BSD-style (see LICENSE)
-- Maintainer  :  Richard Eisenberg (rae@cs.brynmawr.edu)
-- Stability   :  experimental
-- Portability :  portable
--
-- Main entry point into Java interpreter
--
----------------------------------------------------------------------------

module Main where

import Language.Java.Repl

main = repl
